theC64 Package Tool
===================

This tool allows you to encrypt and decrypt files associated with theC64
firmware update files.

Firmware update components
--------------------------

- Package: basic wrapper for an executable.
- Upgrade: extended package that contains an upgrader binary and several
  encrypted payloads.
- Stub: Wrapper around upgrader binary for obfuscation.

Usage
-----

This program takes several commands. You will generally need three keys to run
the commands:

- `hashFill`: This is used to "sign" the package file.
- `keyString`: This is one part of the key for decrypting upgrader binary and
  payload.
- `keyFile`: This is a file used as another part of the key for decrypting
  upgrader binary and payload. This is typically `/usr/lib/libEGL.so` from the
  console.

### `package-create`

Create Package

Creates a new package.

```
Usage:  package-create [arguments]

Arguments:
  hashFill      Hex string of bytes to use in place of hash value when hashing
  version       Package version number in x.x.x format
  execPath      Path of upgrade executable
  outputPath    Path of built package
```

### `package-extract`

Extract Package

Extracts an existing package.

```
Usage:  package-extract [arguments] [options]

Arguments:
  inputPath      Path of package
  outputPath     Path of extracted executable

Options:
  -f|--hashFill  Enables signature checking and sets hex string of bytes to use in place of hash value when hashing
```

### `upgrade-create`

Create Upgrade Package

Creates a new upgrade package.

```
Usage:  upgrade-create [arguments]

Arguments:
  hashFill      Hex string of bytes to use in place of hash value when hashing
  keyString     Hex string of key
  keyFile       Path to key file
  outputPath    Path to build upgrade package
  version       Package version number in x.x.x format
  updaterPath   Path to updater executable
  payloadPath   Path to payload

Options:
  -s|--numSections  Number of sections in package (default 5)
```

### `upgrade-extract`

Extract Upgrade Package

Extracts an existing upgrade package.

```
Usage:  upgrade-extract [arguments] [options]

Arguments:
  keyString      Hex string of key
  keyFile        Path to key file
  inputPath      Path of package
  outputPath     Directory path of extracted files

Options:
  -f|--hashFill  Enables signature checking and sets hex string of bytes to use in place of hash value when hashing
  -s|--numSections  Number of sections in package (default 5)
```

### `stub-create`

Create Encrypted Updater

Creates an encrypted updater binary.

```
Usage:  stub-create [arguments]

Arguments:
  keyString     Hex string of key
  keyFile       Path to key file
  stubPath      Path to decrypter stub
  payloadPath   Path to executable to encrypt
  outputPath    Path to build encrypted updater
```

### `stub-extract`

Extract Encrypted Updater

Extracts an existing encrypted updater binary.

```
Usage:  stub-extract [arguments] [options]

Arguments:
  keyString         Hex string of key
  keyFile           Path to key file
  inputPath         Path to encrypted updater

Options:
  -s|--stubPath     Path to decrypter stub
  -p|--payloadPath  Path to decrypted executable
```

File format description
-----------------------

### Upgrade package

An upgrade package has the following header layout:

```c
struct package_header
{
    unsigned int magic; // 0x64ac64ac
    unsigned int version;
    unsigned char signature[32];
    struct section main_section;
    struct section extended_sections[4];
};
```

- `magic`: Identifies the file as a theC64 upgrade package. `64ac64ac` in little
  endian, or `ac64ac64` in big endian.
- `version`: The version number of the package. A version is consisted of a
  major number, minor number, and revision number. It is stored as each number
  taking up three digits each, then packed into an integer. For example, the
  version number 1.22.333 is packed as integer `001022333`.
- `signature`: A SHA256 hash of the entire file with a hash filler value in this
  field.
- `main_section`: The section definition for the main executable. This
  executable is extracted and run after validating the signature. The firmware
  region is passed to the executable as the first argument. This section is
  never encrypted.
- `extended_sections`: These sections are used by the upgrader for payload
  storage. They are technically an extension of the base package, and not
  required for a valid package.

A section is a blob of data.

```c
struct section
{
    unsigned int offset;
    unsigned int length;
};
```

- `offset`: The offset of the section.
- `length`: The length of the section. A section with zero length is empty.

Note that for extended sections, the offsets are grouped with each other and
lengths grouped with each other, so it's important to know how many sections
there are.

### Stub

A stub is used to encrypt the main executable. It consists of the stub
executable, the encrypted payload, and a 32-bit payload length in big endian
all concatenated together. The key string and key file are used to encrypt the
payload.

#### Encryption

Encryption is performed using Twofish in CFB mode. The IV is stored as the first
16 bytes of the encrypted data. The key is derived from the SHA256 hash of the
key file with the binary key string bytes XORed over the resulting hash. This
encryption is applied to the executable the stub is protecting and the payload
elements.

#### Payload

Update payloads are `.tar.gz` files that can be extracted over the rootfs to
update files.
