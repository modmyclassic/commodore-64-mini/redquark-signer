﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * RedQuarkSigner: theC64 Upgrade Package Tool
 * Copyright (C) 2019, 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using McMaster.Extensions.CommandLineUtils;

namespace RedQuarkSigner
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                FullName = "C64 Mini Upgrade Package Tool"
            };

            // Commands:
            // - create package
            // - extract package
            // - create upgrade
            // - extract upgrade
            // - encrypt stub
            // - decrypt stub

            app.Command("package-create", (config) =>
            {
                config.FullName = "Create Package";
                config.Description = "Creates a new package.";
                var hashFillArg = config.Argument("hashFill", "Hex string of bytes to use in place of hash value when hashing").IsRequired();
                var versionArg = config.Argument("version", "Package version number in x.x.x format").IsRequired();
                var execPathArg = config.Argument("execPath", "Path of upgrade executable").IsRequired();
                execPathArg.Accepts().ExistingFile();
                var outputPathArg = config.Argument("outputPath", "Path of built package").IsRequired();
                outputPathArg.Accepts().LegalFilePath();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    byte[] hashFill = QuarkCrypt.ParseHex(hashFillArg.Value);
                    using (var package = new RedQuarkPackage())
                    {
                        package.HashFiller = hashFill;
                        package.Version = ParseVersionNumber(versionArg.Value);
                        package.SetExecutable(execPathArg.Value);
                        package.WritePackage(outputPathArg.Value);
                    }
                });
            });

            app.Command("package-extract", (config) =>
            {
                config.FullName = "Extract Package";
                config.Description = "Extracts an existing package.";
                var hashFillOpt = config.Option("-f|--hashFill",
                    "Enables signature checking and sets hex string of bytes to use in place of hash value when hashing", CommandOptionType.SingleValue);
                var inputPathArg = config.Argument("inputPath", "Path of package").IsRequired();
                inputPathArg.Accepts().ExistingFile();
                var outputPathArg = config.Argument("outputPath", "Path of extracted executable").IsRequired();
                outputPathArg.Accepts().LegalFilePath();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    using (var package = new RedQuarkPackage(inputPathArg.Value))
                    {
                        if (hashFillOpt.HasValue())
                        {
                            byte[] hashFill = QuarkCrypt.ParseHex(hashFillOpt.Value());
                            package.HashFiller = hashFill;
                            if (!package.CheckSignature()) throw new InvalidDataException("Signature check failed.");
                        }
                        Console.WriteLine("Package version: {0}", package.VersionString);
                        using (FileStream fs = File.Create(outputPathArg.Value))
                        {                            
                            package.Extract(package.ExecutableSection, fs);
                        }
                    }
                });
            });

            app.Command("upgrade-create", (config) =>
            {
                config.FullName = "Create Upgrade Package";
                config.Description = "Creates a new upgrade package.";
                var hashFillArg = config.Argument("hashFill", "Hex string of bytes to use in place of hash value when hashing").IsRequired();
                var keyStringArg = config.Argument("keyString", "Hex string of key").IsRequired();
                var keyFilePathArg = config.Argument("keyFile", "Path to key file").IsRequired();
                keyFilePathArg.Accepts().ExistingFile();
                var outputPathArg = config.Argument("outputPath", "Path to build upgrade package").IsRequired();
                outputPathArg.Accepts().LegalFilePath();
                var versionArg = config.Argument("version", "Package version number in x.x.x format").IsRequired();
                var updaterPathArg = config.Argument("updaterPath", "Path to updater executable").IsRequired();
                updaterPathArg.Accepts().ExistingFile();
                var payloadPathsArg = config.Argument("payloadPath", "Path to payload", true);
                payloadPathsArg.Accepts().ExistingFile();
                var numSectionsOpt = config.Option("--numSections|-s", $"Number of sections in package (default {RedQuarkFirmwareUpgrade.DEFAULT_SECTION_COUNT})",
                    CommandOptionType.SingleValue);
                numSectionsOpt.Accepts().RegularExpression(@"\d+");
                config.HelpOption();

                config.OnExecute(() =>
                {
                    byte[] hashFill = QuarkCrypt.ParseHex(hashFillArg.Value);
                    byte[] keyBytes = QuarkCrypt.ParseHex(keyStringArg.Value);
                    byte[] key = QuarkCrypt.GenerateKey(keyFilePathArg.Value, keyBytes);
                    int numSections = RedQuarkFirmwareUpgrade.DEFAULT_SECTION_COUNT;
                    if (numSectionsOpt.HasValue())
                    {
                        if (!int.TryParse(numSectionsOpt.Value(), out var parsedValue))
                            throw new CommandParsingException(config, "Could not parse number of sections.");
                        numSections = parsedValue;
                    }

                    using (var package = new RedQuarkFirmwareUpgrade(numSections))
                    {
                        package.HashFiller = hashFill;
                        package.EncryptionKey = key;
                        package.Version = ParseVersionNumber(versionArg.Value);
                        package.SetExecutable(updaterPathArg.Value);
                        foreach (var path in payloadPathsArg.Values)
                        {
                            package.AddSection(path);
                        }
                        package.WritePackage(outputPathArg.Value);
                    }
                });
            });

            app.Command("upgrade-extract", (config) =>
            {
                config.FullName = "Extract Upgrade Package";
                config.Description = "Extracts an existing upgrade package.";
                var hashFillOpt = config.Option("-f|--hashFill",
                    "Enables signature checking and sets hex string of bytes to use in place of hash value when hashing", CommandOptionType.SingleValue);
                var keyStringArg = config.Argument("keyString", "Hex string of key").IsRequired();
                var keyFilePathArg = config.Argument("keyFile", "Path to key file").IsRequired();
                keyFilePathArg.Accepts().ExistingFile();
                var inputPathArg = config.Argument("inputPath", "Path of package").IsRequired();
                inputPathArg.Accepts().ExistingFile();
                var outputPathArg = config.Argument("outputPath", "Directory path of extracted files").IsRequired();
                outputPathArg.Accepts().LegalFilePath();
                var numSectionsOpt = config.Option("--numSections|-s", $"Number of sections in package (default {RedQuarkFirmwareUpgrade.DEFAULT_SECTION_COUNT})",
                    CommandOptionType.SingleValue);
                numSectionsOpt.Accepts().RegularExpression(@"\d+");
                config.HelpOption();

                config.OnExecute(() =>
                {
                    byte[] keyBytes = QuarkCrypt.ParseHex(keyStringArg.Value);
                    byte[] key = QuarkCrypt.GenerateKey(keyFilePathArg.Value, keyBytes);
                    int numSections = RedQuarkFirmwareUpgrade.DEFAULT_SECTION_COUNT;
                    if (numSectionsOpt.HasValue())
                    {
                        if (!int.TryParse(numSectionsOpt.Value(), out var parsedValue))
                            throw new CommandParsingException(config, "Could not parse number of sections.");
                        numSections = parsedValue;
                    }

                    using (var package = new RedQuarkFirmwareUpgrade(inputPathArg.Value, numSections))
                    {
                        if (hashFillOpt.HasValue())
                        {
                            byte[] hashFill = QuarkCrypt.ParseHex(hashFillOpt.Value());
                            package.HashFiller = hashFill;
                            if (!package.CheckSignature()) throw new InvalidDataException("Signature check failed.");
                        }
                        Console.WriteLine("Package version: {0}", package.VersionString);
                        package.EncryptionKey = key;
                        string outDirPath = outputPathArg.Value;
                        Directory.CreateDirectory(outDirPath);
                        using (FileStream fs = File.Create(Path.Combine(outDirPath, "update")))
                        {
                            package.Extract(package.ExecutableSection, fs);
                        }
                        var updateSections = package.UpdateSections;
                        for (int i = 0; i < updateSections.Count; ++i)
                        {
                            if (updateSections[i].Length != 0)
                            {
                                using (FileStream fs = File.Create(Path.Combine(outDirPath, i + ".tar.gz")))
                                {
                                    package.Extract(updateSections[i], fs);
                                }
                            }
                        }
                    }
                });
            });

            app.Command("stub-create", (config) =>
            {
                config.FullName = "Create Encrypted Updater";
                config.Description = "Creates an encrypted updater binary.";
                var keyStringArg = config.Argument("keyString", "Hex string of key").IsRequired();
                var keyFilePathArg = config.Argument("keyFile", "Path to key file").IsRequired();
                keyFilePathArg.Accepts().ExistingFile();
                var stubPathArg = config.Argument("stubPath", "Path to decrypter stub").IsRequired();
                stubPathArg.Accepts().ExistingFile();
                var payloadPathArg = config.Argument("payloadPath", "Path to executable to encrypt").IsRequired();
                payloadPathArg.Accepts().ExistingFile();
                var outputPathArg = config.Argument("outputPath", "Path to build encrypted updater").IsRequired();
                outputPathArg.Accepts().LegalFilePath();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    byte[] keyBytes = QuarkCrypt.ParseHex(keyStringArg.Value);
                    byte[] key = QuarkCrypt.GenerateKey(keyFilePathArg.Value, keyBytes);
                    CryptStub.Build(key, stubPathArg.Value, payloadPathArg.Value, outputPathArg.Value);
                });
            });

            app.Command("stub-extract", (config) =>
            {
                config.FullName = "Extract Encrypted Updater";
                config.Description = "Extracts an existing encrypted updater binary.";
                var keyStringArg = config.Argument("keyString", "Hex string of key").IsRequired();
                var keyFilePathArg = config.Argument("keyFile", "Path to key file").IsRequired();
                keyFilePathArg.Accepts().ExistingFile();
                var inputPathArg = config.Argument("inputPath", "Path to encrypted updater").IsRequired();
                inputPathArg.Accepts().ExistingFile();
                var stubPathOpt = config.Option("-s|--stubPath", "Path to decrypter stub", CommandOptionType.SingleValue);
                stubPathOpt.Accepts().LegalFilePath();
                var payloadPathOpt = config.Option("-p|--payloadPath", "Path to decrypted executable", CommandOptionType.SingleValue);
                payloadPathOpt.Accepts().LegalFilePath();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    byte[] keyBytes = QuarkCrypt.ParseHex(keyStringArg.Value);
                    byte[] key = QuarkCrypt.GenerateKey(keyFilePathArg.Value, keyBytes);
                    CryptStub.Extract(key, inputPathArg.Value, stubPathOpt.Value(), payloadPathOpt.Value());
                });
            });

            app.VersionOptionFromAssemblyAttributes(System.Reflection.Assembly.GetExecutingAssembly());
            app.HelpOption();

            app.OnExecute(() =>
            {
                app.ShowHelp();
                return 1;
            });

            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.Error.WriteLine(ex.Message);
                Environment.Exit(1);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error while processing: {0}", ex);
                Environment.Exit(-1);
            }
        }

        static uint ParseVersionNumber(string versionString)
        {
            if (string.IsNullOrEmpty(versionString)) throw new ArgumentNullException(nameof(versionString));

            int major = 0;
            int minor = 0;
            int rev = 0;

            string[] split = versionString.Split('.');
            if (split.Length > 3) throw new ArgumentException("Version string has too many components.", nameof(versionString));
            if (split.Length > 0) major = int.Parse(split[0]);
            if (split.Length > 1) minor = int.Parse(split[1]);
            if (split.Length > 2) rev = int.Parse(split[2]);

            return (uint)(major * 1000000 + minor * 1000 + rev);
        }
    }
}
