﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * RedQuarkSigner: theC64 Upgrade Package Tool
 * Copyright (C) 2019, 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace RedQuarkSigner
{
    /// <summary>
    /// Represents a theC64 upgrade package.
    /// </summary>
    /// <remarks>This type of package can hold at most 4 sections in addition to the main executable section.</remarks>
    public class RedQuarkFirmwareUpgrade : RedQuarkPackage
    {
        public static readonly int DEFAULT_SECTION_COUNT = 5;

        List<Section> updateSections = new List<Section>();

        /// <summary>
        /// Gets the sections in this package.
        /// </summary>
        public IReadOnlyList<Section> UpdateSections => new ReadOnlyCollection<Section>(updateSections);
        /// <summary>
        /// Gets or sets the encryption key for the payload in this package.
        /// </summary>
        public byte[] EncryptionKey { get; set; }
        /// <summary>
        /// Gets the number of sections in this package.
        /// </summary>
        public int NumSections { get; private set; }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for writing.
        /// </summary>
        public RedQuarkFirmwareUpgrade()
        {
            NumSections = DEFAULT_SECTION_COUNT;
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for reading.
        /// </summary>
        /// <param name="path">The path to the package to open.</param>
        public RedQuarkFirmwareUpgrade(string path) : base(path)
        {
            NumSections = DEFAULT_SECTION_COUNT;
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for reading.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="stream"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="stream"/> is not seekable.</exception>
        public RedQuarkFirmwareUpgrade(Stream stream) : base(stream)
        {
            NumSections = DEFAULT_SECTION_COUNT;
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for writing.
        /// </summary>
        /// <param name="sections">The number of sections in the package.</param>
        public RedQuarkFirmwareUpgrade(int sections)
        {
            NumSections = sections;
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for reading.
        /// </summary>
        /// <param name="path">The path to the package to open.</param>
        /// <param name="sections">The number of sections in the package.</param>
        public RedQuarkFirmwareUpgrade(string path, int sections) : base(path)
        {
            NumSections = sections;
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkFirmwareUpgrade"/> for reading.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <param name="sections">The number of sections in the package.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="stream"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="stream"/> is not seekable.</exception>
        public RedQuarkFirmwareUpgrade(Stream stream, int sections) : base(stream)
        {
            NumSections = sections;
        }

        /// <summary>
        /// Adds a payload section for writing to the package.
        /// </summary>
        /// <param name="path">The path to the payload to include.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="path"/> is <c>null</c>.</exception>
        /// <exception cref="InvalidOperationException">If maximum number of sections have been added.</exception>
        public void AddSection(string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (updateSections.Count >= NumSections)
                throw new InvalidOperationException("Maximum number of sections already reached.");
            updateSections.Add(new Section(this)
            {
                Path = path
            });
        }

        /// <summary>
        /// Removes a section from this package.
        /// </summary>
        /// <param name="section">The section to remove.</param>
        /// <returns>Whether a matching section was removed.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="section"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">
        /// If <paramref name="section"/> is not from this package or if it is for the main executable.
        /// </exception>
        public bool RemoveSection(Section section)
        {
            if (section == null) throw new ArgumentNullException(nameof(section));
            if (section.parent != this) throw new ArgumentException("Section is not from this package.", nameof(section));
            if (section == ExecutableSection) throw new ArgumentException("Section is for main executable.", nameof(section));
            section.parent = null;
            return updateSections.Remove(section);
        }

        /// <inheritdoc/>
        public override void Extract(Section section, Stream outStream)
        {
            if (section == ExecutableSection || section != null && !string.IsNullOrEmpty(section.Path))
            {
                base.Extract(section, outStream);
            }
            else
            {
                if (section == null) throw new ArgumentNullException(nameof(section));
                if (outStream == null) throw new ArgumentNullException(nameof(outStream));
                if (section.parent != this) throw new ArgumentException("Section is not from this package.", nameof(section));
                if (EncryptionKey == null || EncryptionKey.Length != 16)
                    throw new InvalidOperationException("Valid encryption key has not been set.");

                CheckCanRead();
                stream.Seek(section.Offset, SeekOrigin.Begin);
                QuarkCrypt.ReadCryptedData(EncryptionKey, stream, outStream, (int)section.Length);
            }
        }

        /// <inheritdoc/>
        protected override void WriteSection(Section section, Stream outStream)
        {
            if (section == ExecutableSection)
            {
                base.WriteSection(section, outStream);
            }
            else
            {
                if (section == null) throw new ArgumentNullException(nameof(section));
                if (outStream == null) throw new ArgumentNullException(nameof(outStream));
                if (section.parent != this) throw new ArgumentException("Section is not from this package.", nameof(section));
                if (EncryptionKey == null || EncryptionKey.Length != 16)
                    throw new InvalidOperationException("Valid encryption key has not been set.");

                if (!string.IsNullOrEmpty(section.Path))
                {
                    using (FileStream fs = File.OpenRead(section.Path))
                    {
                        QuarkCrypt.WriteCryptedData(EncryptionKey, fs, outStream, (int)fs.Length);
                    }
                }
                else
                {
                    CheckCanRead();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        Extract(section, ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        QuarkCrypt.WriteCryptedData(EncryptionKey, ms, outStream, (int)ms.Length);
                    }
                }
            }
        }

        /// <inheritdoc/>
        protected override int ReserveHeaderSpace()
        {
            return NumSections * 8;
        }

        /// <inheritdoc/>
        protected override void WriteExtendedHeader(Stream outStream, object context)
        {
            var outSections = (List<Section>)context;
            int writtenCount = 0;
            BinaryWriter bw = new BinaryWriter(outStream);
            foreach (var section in outSections)
            {
                bw.Write(section.Offset);
                ++writtenCount;
            }
            for (; writtenCount < NumSections; ++writtenCount)
            {
                bw.Write((uint)outStream.Length);
            }
            writtenCount = 0;
            foreach (var section in outSections)
            {
                bw.Write(section.Length);
                ++writtenCount;
            }
            for (; writtenCount < NumSections; ++writtenCount)
            {
                bw.Write(0);
            }
        }

        /// <inheritdoc/>
        protected override object WriteExtendedData(Stream outStream)
        {
            var outSections = new List<Section>();
            foreach (var section in updateSections)
            {
                Section newSection = new Section(this)
                {
                    Offset = (uint)outStream.Position
                };
                WriteSection(section, outStream);
                newSection.Length = (uint)(outStream.Position - newSection.Offset);
                outSections.Add(newSection);
            }
            return outSections;
        }

        /// <inheritdoc/>
        protected override void ParseExtendedHeader()
        {
            uint[] offsets = new uint[NumSections];
            uint[] lengths = new uint[NumSections];
            for (int i = 0; i < NumSections; ++i)
            {
                offsets[i] = br.ReadUInt32();
            }
            for (int i = 0; i < NumSections; ++i)
            {
                lengths[i] = br.ReadUInt32();
            }
            for (int i = 0; i < NumSections; ++i)
            {
                updateSections.Add(new Section(this)
                {
                    Offset = offsets[i],
                    Length = lengths[i]
                });
            }
        }
    }
}
