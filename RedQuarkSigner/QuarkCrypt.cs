﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * RedQuarkSigner: theC64 Upgrade Package Tool
 * Copyright (C) 2019, 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Modes;
using GMWare.IO;

namespace RedQuarkSigner
{
    /// <summary>
    /// A collection of functions for encrypting payload.
    /// </summary>
    public static class QuarkCrypt
    {
        static RandomNumberGenerator secureRandom;

        static RandomNumberGenerator SecureRandom
        {
            get
            {
                if (secureRandom == null)
                {
                    secureRandom = RandomNumberGenerator.Create();
                }
                return secureRandom;
            }
        }

        /// <summary>
        /// Generates key for encrypting payload.
        /// </summary>
        /// <param name="hashFile">The <see cref="Stream"/> to hash.</param>
        /// <param name="mungeBytes">The bytes to XOR over the hash of <paramref name="hashFile"/>.</param>
        /// <returns>The generated key.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="hashFile"/> or <paramref name="mungeBytes"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If <paramref name="mungeBytes"/> is not exactly 16 bytes in length.
        /// </exception>
        public static byte[] GenerateKey(Stream hashFile, byte[] mungeBytes)
        {
            if (hashFile == null) throw new ArgumentNullException(nameof(hashFile));
            if (mungeBytes == null) throw new ArgumentNullException(nameof(mungeBytes));
            if (mungeBytes.Length != 16) throw new ArgumentException("Key bytes must be 16 bytes in length.", nameof(mungeBytes));
            byte[] key = (byte[])mungeBytes.Clone();
            using (SHA256 sha = SHA256.Create())
            {
                byte[] hash = sha.ComputeHash(hashFile);
                string hexString = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
                byte[] hexHash = Encoding.ASCII.GetBytes(hexString);
                for (int i = 0; i < key.Length; ++i)
                {
                    key[i] ^= hexHash[i];
                }
            }
            return key;
        }

        /// <summary>
        /// Generates key for encrypting payload.
        /// </summary>
        /// <param name="hashPath">The path to the file to hash.</param>
        /// <param name="mungeBytes">The bytes to XOR over the hash of <paramref name="hashPath"/>.</param>
        /// <returns>The generated key.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="hashPath"/> or <paramref name="mungeBytes"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If <paramref name="mungeBytes"/> is not exactly 16 bytes in length.
        /// </exception>
        public static byte[] GenerateKey(string hashPath, byte[] mungeBytes)
        {
            if (hashPath == null) throw new ArgumentNullException(nameof(hashPath));
            if (mungeBytes == null) throw new ArgumentNullException(nameof(mungeBytes));
            using (FileStream fs = File.OpenRead(hashPath))
            {
                return GenerateKey(fs, mungeBytes);
            }
        }

        /// <summary>
        /// Creates a Twofish CFB cipher.
        /// </summary>
        /// <param name="key">The cipher key.</param>
        /// <param name="iv">The cipher IV.</param>
        /// <param name="forEncryption">
        /// <c>true</c> when using for encryption, <c>false</c> when using for decryption.
        /// </param>
        /// <returns>The created cipher.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="key"/> or <paramref name="iv"/> is <c>null</c>.
        /// </exception>
        public static IBufferedCipher GetTwofishCfbCipher(byte[] key, byte[] iv, bool forEncryption)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (iv == null) throw new ArgumentNullException(nameof(iv));
            var engine = new TwofishEngine();
            var cfb = new CfbBlockCipher(engine, engine.GetBlockSize() * 8);
            var buffered = new BufferedBlockCipher(cfb);
            var keyParams = new KeyParameter(key);
            var keyWithIv = new ParametersWithIV(keyParams, iv);
            buffered.Init(forEncryption, keyWithIv);
            return buffered;
        }

        /// <summary>
        /// Processes a <see cref="Stream"/> through a <see cref="IBufferedCipher"/>.
        /// </summary>
        /// <param name="cipher">The cipher to use.</param>
        /// <param name="from">The stream to copy and apply the cipher from.</param>
        /// <param name="to">The stream to copy to.</param>
        /// <param name="length">The length of the data to process.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="cipher"/>, <paramref name="from"/>, or <paramref name="to"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is negative.</exception>
        public static void CryptCopy(IBufferedCipher cipher, Stream from, Stream to, int length)
        {
            if (cipher == null) throw new ArgumentNullException(nameof(cipher));
            if (from == null) throw new ArgumentNullException(nameof(from));
            if (to == null) throw new ArgumentNullException(nameof(to));
            if (length < 0) throw new ArgumentOutOfRangeException(nameof(length), "Length must be positive.");

            byte[] buffer = new BinaryReader(from).ReadBytes(length);
            int blockSize = cipher.GetBlockSize();
            Array.Resize(ref buffer, (buffer.Length + blockSize - 1) / blockSize * blockSize);
            byte[] processed = cipher.ProcessBytes(buffer);
            Array.Resize(ref processed, length);
            to.Write(processed, 0, processed.Length);
        }

        /// <summary>
        /// Decrypts Twofish encrypted data from a <see cref="Stream"/>.
        /// </summary>
        /// <param name="key">The key to use for the Twofish cipher.</param>
        /// <param name="from">The stream to read from.</param>
        /// <param name="to">The stream to write decrypted data to.</param>
        /// <param name="length">The length of the encrypted data.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="key"/>, <paramref name="from"/>, or <paramref name="to"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">If <paramref name="key"/> is not exactly 16 bytes long.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is not at least 16.</exception>
        public static void ReadCryptedData(byte[] key, Stream from, Stream to, int length)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (key.Length != 16) throw new ArgumentException("Key must be 16 bytes long.", nameof(key));
            if (length < 16) throw new ArgumentOutOfRangeException(nameof(length), "Length must be at least 16.");

            BinaryReader br = new BinaryReader(from);
            byte[] iv = br.ReadBytes(16);
            var cipher = GetTwofishCfbCipher(key, iv, false);
            CryptCopy(cipher, from, to, length - 16);
        }

        /// <summary>
        /// Encrypts Twofish encrypted data from a <see cref="Stream"/>.
        /// </summary>
        /// <param name="key">The key to use for the Twofish cipher.</param>
        /// <param name="from">The stream to read from.</param>
        /// <param name="to">The stream to write encrypted data to.</param>
        /// <param name="length">The length of the data.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="key"/>, <paramref name="from"/>, or <paramref name="to"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">If <paramref name="key"/> is not exactly 16 bytes long.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is negative.</exception>
        public static void WriteCryptedData(byte[] key, Stream from, Stream to, int length)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (key.Length != 16) throw new ArgumentException("Key must be 16 bytes long.", nameof(key));
            if (length < 0) throw new ArgumentOutOfRangeException(nameof(length), "Length must be positive.");

            byte[] iv = new byte[16];
            SecureRandom.GetBytes(iv);
            var cipher = GetTwofishCfbCipher(key, iv, true);
            new BinaryWriter(to).Write(iv);
            CryptCopy(cipher, from, to, length);
        }

        /// <summary>
        /// Parses a hex string into bytes.
        /// </summary>
        /// <param name="hexString">The string to parse.</param>
        /// <returns>The parsed bytes.</returns>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="hexString"/> is <c>null</c> or empty.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If <paramref name="hexString"/> does not have an even length.
        /// </exception>
        public static byte[] ParseHex(string hexString)
        {
            if (string.IsNullOrEmpty(hexString)) throw new ArgumentNullException(nameof(hexString));
            if (hexString.Length % 2 != 0) throw new ArgumentException("Invalid string length.", nameof(hexString));

            hexString = hexString.ToUpper();
            if (hexString.StartsWith("0X")) hexString = hexString.Substring(2);
            byte[] bytes = new byte[hexString.Length / 2];
            for (int i = 0; i < hexString.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            }
            return bytes;
        }
    }
}
