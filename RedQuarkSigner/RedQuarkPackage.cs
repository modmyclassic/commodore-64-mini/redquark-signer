﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * RedQuarkSigner: theC64 Upgrade Package Tool
 * Copyright (C) 2019, 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using GMWare.IO;

namespace RedQuarkSigner
{
    /// <summary>
    /// Represents a theC64 executable package.
    /// </summary>
    public class RedQuarkPackage : IDisposable
    {
        static readonly uint MAGIC = 0x64ac64ac;

        /// <summary>
        /// Represents an individual file within a package.
        /// </summary>
        public class Section
        {
            /// <summary>
            /// Gets the path on disk of the file. If <c>null</c>, the file is from an existing package.
            /// </summary>
            public string Path { get; internal set; }
            /// <summary>
            /// Gets the offset of the file from an existing package.
            /// </summary>
            public uint Offset { get; internal set; }
            /// <summary>
            /// Gets the length of the file from an existing package.
            /// </summary>
            public uint Length { get; internal set; }

            internal RedQuarkPackage parent;

            /// <summary>
            /// Instantiates a new instance of <see cref="Section"/>.
            /// </summary>
            /// <param name="parent"></param>
            internal Section(RedQuarkPackage parent)
            {
                this.parent = parent;
            }
        }

        protected Stream stream;
        protected BinaryReader br;
        bool disposed;

        /// <summary>
        /// Gets or sets the version number of the package.
        /// </summary>
        public uint Version { get; set; }
        /// <summary>
        /// Gets the version number as a formatted string.
        /// </summary>
        public string VersionString => $"{Version / 1000000}.{Version / 1000 % 1000}.{Version % 1000}";
        /// <summary>
        /// Gets the <see cref="Section"/> of the executable payload.
        /// </summary>
        public Section ExecutableSection { get; private set; }
        /// <summary>
        /// Gets or sets the filler bytes used when calculating signature.
        /// </summary>
        public byte[] HashFiller { get; set; }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkPackage"/> for writing.
        /// </summary>
        public RedQuarkPackage()
        { }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkPackage"/> for reading.
        /// </summary>
        /// <param name="path">The path to the package to open.</param>
        public RedQuarkPackage(string path) : this(File.OpenRead(path))
        { }

        /// <summary>
        /// Instantiates a new instance of <see cref="RedQuarkPackage"/> for reading.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="stream"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="stream"/> is not seekable.</exception>
        public RedQuarkPackage(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (!stream.CanSeek) throw new ArgumentException("Stream must be seekable.", nameof(stream));
            this.stream = stream;
            br = new BinaryReader(stream);
            ParseHeader();
            ParseExtendedHeader();
        }

        /// <summary>
        /// Sets the version of the package by component.
        /// </summary>
        /// <param name="major">The major version.</param>
        /// <param name="minor">The minor version.</param>
        /// <param name="revision">The revision.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when any of the components are out of range.</exception>
        public void SetVersion(int major, int minor, int revision)
        {
            if (major < 0) throw new ArgumentOutOfRangeException(nameof(major), "Major version must be positive.");
            if (minor < 0 || minor >= 1000) throw new ArgumentOutOfRangeException(nameof(minor), "Minor version must be between 0 and 999 inclusive.");
            if (revision < 0 || revision >= 1000) throw new ArgumentOutOfRangeException(nameof(revision), "Revision must be between 0 and 999 inclusive.");
            Version = (uint)(major * 1000000 + minor * 1000 + revision);
        }

        /// <summary>
        /// Sets the path of the executable payload for writing.
        /// </summary>
        /// <param name="path">The path of the payload.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> is <c>null</c> or empty.</exception>
        public void SetExecutable(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (ExecutableSection != null) ExecutableSection.parent = null;
            ExecutableSection = new Section(this)
            {
                Path = path
            };
        }

        /// <summary>
        /// Throws an exception if the object is disposed.
        /// </summary>
        protected void CheckDisposed()
        {
            if (disposed) throw new ObjectDisposedException(GetType().FullName);
        }

        /// <summary>
        /// Throws an exception if the package is not available to be read.
        /// </summary>
        protected void CheckCanRead()
        {
            CheckDisposed();
            if (stream == null || !stream.CanRead) throw new InvalidOperationException("Stream is not readable.");
        }

        void ParseHeader()
        {
            stream.Seek(0, SeekOrigin.Begin);
            if (br.ReadUInt32() != MAGIC) throw new InvalidDataException("File is not a C64 Mini upgrade package.");
            Version = br.ReadUInt32();
            // Skip signature
            stream.Seek(0x20, SeekOrigin.Current);
            // Read executable section
            ExecutableSection = new Section(this)
            {
                Offset = br.ReadUInt32(),
                Length = br.ReadUInt32()
            };
        }

        /// <summary>
        /// Parses extended header.
        /// </summary>
        protected virtual void ParseExtendedHeader()
        { }

        /// <summary>
        /// Copies the contents of a section to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="section">The <see cref="Section"/> to copy from.</param>
        /// <param name="outStream">The <see cref="Stream"/> to copy to.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="section"/> or
        /// <paramref name="outStream"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="section"/> is not
        /// from this package.</exception>
        protected void BasicSectionCopy(Section section, Stream outStream)
        {
            if (section == null) throw new ArgumentNullException(nameof(section));
            if (outStream == null) throw new ArgumentNullException(nameof(outStream));
            if (section.parent != this) throw new ArgumentException("Section is not from this package.", nameof(section));

            if (string.IsNullOrEmpty(section.Path))
            {
                // Existing package
                CheckCanRead();
                stream.Seek(section.Offset, SeekOrigin.Begin);
                StreamUtils.StreamCopy(stream, outStream, section.Length);
            }
            else
            {
                // Some file on disk
                using (FileStream fs = File.OpenRead(section.Path))
                {
                    fs.CopyTo(outStream);
                }
            }
        }

        /// <summary>
        /// Extracts a section to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="section">The <see cref="Section"/> to copy from.</param>
        /// <param name="outStream">The <see cref="Stream"/> to copy to.</param>
        public virtual void Extract(Section section, Stream outStream)
        {
            BasicSectionCopy(section, outStream);
        }

        /// <summary>
        /// Writes a section to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="section">The <see cref="Section"/> to copy from.</param>
        /// <param name="outStream">The <see cref="Stream"/> to copy to.</param>
        protected virtual void WriteSection(Section section, Stream outStream)
        {
            BasicSectionCopy(section, outStream);
        }

        /// <summary>
        /// Generates the signature of a <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to generate a signature from.</param>
        /// <param name="hashFiller">The bytes to use in place of the hash.</param>
        /// <returns>The signature bytes.</returns>
        /// <exception cref="ArgumentNullException">
        /// If the <paramref name="stream"/> or <paramref name="hashFiller"/> is <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">If <paramref name="hashFiller"/> is not exactly 20 bytes long.</exception>
        static protected byte[] MakeSignature(Stream stream, byte[] hashFiller)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (hashFiller == null) throw new ArgumentNullException(nameof(hashFiller));
            if (hashFiller.Length != 0x20) throw new ArgumentException("Hash filled needs to be exactly 20 bytes long.", nameof(hashFiller));
            BinaryReader br = new BinaryReader(stream);
            using (SHA256 sha = SHA256.Create())
            {
                stream.Seek(0, SeekOrigin.Begin);
                byte[] buffer = br.ReadBytes(0x30);
                Buffer.BlockCopy(hashFiller, 0, buffer, 8, 0x20);
                sha.TransformBlock(buffer, 0, buffer.Length, buffer, 0);
                StreamUtils.StreamCopy(stream, Stream.Null, (buf, read) =>
                {
                    sha.TransformBlock(buf, 0, read, buf, 0);
                    return true;
                });
                sha.TransformFinalBlock(new byte[0], 0, 0);
                return sha.Hash;
            }
        }

        /// <summary>
        /// Verifies the signature of the package.
        /// </summary>
        /// <returns>Whether the signature calculated matches the signature in the package.</returns>
        public bool CheckSignature()
        {
            CheckCanRead();
            byte[] calculated = MakeSignature(stream, HashFiller);
            stream.Seek(8, SeekOrigin.Begin);
            byte[] comparing = br.ReadBytes(0x20);
            return comparing.SequenceEqual(calculated);
        }

        /// <summary>
        /// Writes the package to a file.
        /// </summary>
        /// <param name="path">The path of the file to write to.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="path"/> is <c>null</c>.</exception>
        public void WritePackage(string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            CheckDisposed();
            using (FileStream fs = File.Create(path))
            {
                WritePackage(fs);
            }
        }

        /// <summary>
        /// Writes the package to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="outStream">The stream to write the package to.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="outStream"/> is <c>null</c>.</exception>
        /// <exception cref="InvalidOperationException">If there is no executable section set.</exception>
        public void WritePackage(Stream outStream)
        {
            if (outStream == null) throw new ArgumentNullException(nameof(outStream));
            CheckDisposed();
            if (ExecutableSection == null) throw new InvalidOperationException("Executable must be set first.");
            BinaryWriter bw = new BinaryWriter(outStream);
            outStream.Seek(0, SeekOrigin.Begin);
            bw.Write(MAGIC);
            bw.Write(Version);
            // Reserve signature space
            bw.Write(new byte[0x20]);
            // Reserve executable section space
            bw.Write(0);
            bw.Write(0);
            // Reserve header space
            int extraHeaderSpace = ReserveHeaderSpace();
            bw.Write(new byte[extraHeaderSpace]);
            // Write executable data
            Section outExecutableSection = new Section(this)
            {
                Offset = (uint)outStream.Position
            };
            WriteSection(ExecutableSection, outStream);
            outExecutableSection.Length = (uint)(outStream.Position - outExecutableSection.Offset);
            // Write extended data
            var extendedContext = WriteExtendedData(outStream);
            // Write executable section
            outStream.Seek(0x28, SeekOrigin.Begin);
            bw.Write(outExecutableSection.Offset);
            bw.Write(outExecutableSection.Length);
            // Write extended header
            WriteExtendedHeader(outStream, extendedContext);
            // Generate signature
            byte[] sig = MakeSignature(outStream, HashFiller);
            // Write signature
            outStream.Seek(8, SeekOrigin.Begin);
            bw.Write(sig);
        }

        /// <summary>
        /// Reserves space in the header for additional sections.
        /// </summary>
        /// <returns>The number of bytes reserved.</returns>
        protected virtual int ReserveHeaderSpace()
        {
            return 0;
        }

        /// <summary>
        /// Writes extended package header.
        /// </summary>
        /// <param name="outStream">The stream to write to.</param>
        /// <param name="context">The context object returned by <see cref="WriteExtendedData(Stream)"/>.</param>
        protected virtual void WriteExtendedHeader(Stream outStream, object context)
        { }

        /// <summary>
        /// Writes extended package data.
        /// </summary>
        /// <param name="outStream">The stream to write to.</param>
        /// <returns>A context object for passing into <see cref="WriteExtendedHeader(Stream, object)"/>.</returns>
        protected virtual object WriteExtendedData(Stream outStream)
        {
            return null;
        }

        /// <summary>
        /// Releases the resources used by the package.
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources;
        /// <c>false</c> to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (stream != null) stream.Dispose();
            disposed = true;
        }

        /// <summary>
        /// Releases the resources used by the package.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }
    }
}
