﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * RedQuarkSigner: theC64 Upgrade Package Tool
 * Copyright (C) 2019, 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GMWare.IO;

namespace RedQuarkSigner
{
    /// <summary>
    /// A collection of functions for manipulating encryption stubs.
    /// </summary>
    public static class CryptStub
    {
        /// <summary>
        /// Builds an encrypted stub.
        /// </summary>
        /// <param name="key">The key used for encrypting.</param>
        /// <param name="stubPath">The path of the stub executable.</param>
        /// <param name="payloadPath">The path of the payload executable.</param>
        /// <param name="outputPath">The path to write the built stub to.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="key"/> is <c>null</c>, or if <paramref name="stubPath"/>,
        /// <paramref name="payloadPath"/>, or <paramref name="outputPath"/> is <c>null</c>
        /// or empty.
        /// </exception>
        public static void Build(byte[] key, string stubPath, string payloadPath, string outputPath)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrEmpty(stubPath)) throw new ArgumentNullException(nameof(stubPath));
            if (string.IsNullOrEmpty(payloadPath)) throw new ArgumentNullException(nameof(payloadPath));
            if (string.IsNullOrEmpty(outputPath)) throw new ArgumentNullException(nameof(outputPath));
            using (var stubStream = File.OpenRead(stubPath))
            using (var payloadStream = File.OpenRead(payloadPath))
            using (var outputStream = File.Create(outputPath))
            {
                Build(key, stubStream, payloadStream, outputStream);
            }
        }

        /// <summary>
        /// Builds an encrypted stub.
        /// </summary>
        /// <param name="key">The key used for encrypting.</param>
        /// <param name="stubStream">The stream to read the stub from.</param>
        /// <param name="payloadStream">The stream to read the payload from.</param>
        /// <param name="outputStream">The stream to write the built stub to.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="key"/>, <paramref name="stubStream"/>, <paramref name="payloadStream"/>,
        /// or <paramref name="outputStream"/> is <c>null</c>.
        /// </exception>
        public static void Build(byte[] key, Stream stubStream, Stream payloadStream, Stream outputStream)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (stubStream == null) throw new ArgumentNullException(nameof(stubStream));
            if (payloadStream == null) throw new ArgumentNullException(nameof(payloadStream));
            if (outputStream == null) throw new ArgumentNullException(nameof(outputStream));

            stubStream.CopyTo(outputStream);
            QuarkCrypt.WriteCryptedData(key, payloadStream, outputStream, (int)payloadStream.Length);
            uint payloadLength = (uint)(payloadStream.Length + 16);
            outputStream.WriteByte((byte)(payloadLength >> 24));
            outputStream.WriteByte((byte)(payloadLength >> 16));
            outputStream.WriteByte((byte)(payloadLength >> 8));
            outputStream.WriteByte((byte)payloadLength);
        }

        /// <summary>
        /// Extracts a built stub.
        /// </summary>
        /// <param name="key">The key used for decrypting.</param>
        /// <param name="inPath">The path to the built stub.</param>
        /// <param name="stubPath">The path to the stub executable to write. Pass <c>null</c> to skip writing it.</param>
        /// <param name="payloadPath">The path to the payload to write. Pass <c>null</c> to skip writing it.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="inPath"/> is <c>null</c> or empty, or if <paramref name="payloadPath"/> is
        /// supplied but <paramref name="key"/> is <c>null</c>.
        /// </exception>
        public static void Extract(byte[] key, string inPath, string stubPath, string payloadPath)
        {
            if (key == null && !string.IsNullOrEmpty(payloadPath)) throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrEmpty(inPath)) throw new ArgumentNullException(nameof(inPath));

            using (var inStream = File.OpenRead(inPath))
            using (var stubStream = string.IsNullOrEmpty(stubPath) ? null : File.Create(stubPath))
            using (var payloadStream = string.IsNullOrEmpty(payloadPath) ? null : File.Create(payloadPath))
            {
                Extract(key, inStream, stubStream, payloadStream);
            }
        }

        /// <summary>
        /// Extracts a built stub.
        /// </summary>
        /// <param name="key">The key used for decrypting.</param>
        /// <param name="inStream">The stream of the built stub to read from.</param>
        /// <param name="stubStream">The stream to write the stub executable to. Pass <c>null</c> to skip writing it.</param>
        /// <param name="payloadStream">The stream to write the payload to. Pass <c>null</c> to skip writing it.</param>
        /// <exception cref="ArgumentNullException">
        /// If <paramref name="inStream"/> is <c>null</c>, or if <paramref name="payloadStream"/> is
        /// supplied but <paramref name="key"/> is <c>null</c>.
        /// </exception>
        public static void Extract(byte[] key, Stream inStream, Stream stubStream, Stream payloadStream)
        {
            if (key == null && payloadStream != null) throw new ArgumentNullException(nameof(key));
            if (inStream == null) throw new ArgumentNullException(nameof(inStream));

            inStream.Seek(-4, SeekOrigin.End);
            BinaryReader br = new BinaryReader(inStream);
            uint payloadLength = unchecked((uint)((br.ReadByte() << 24) | (br.ReadByte() << 16) | (br.ReadByte() << 8) | br.ReadByte()));
            if (stubStream != null)
            {
                inStream.Seek(0, SeekOrigin.Begin);
                StreamUtils.StreamCopy(inStream, stubStream, inStream.Length - payloadLength - 4);
            }
            else
            {
                inStream.Seek(-payloadLength - 4, SeekOrigin.End);
            }
            if (payloadStream != null)
            {
                QuarkCrypt.ReadCryptedData(key, inStream, payloadStream, (int)payloadLength);
            }
        }
    }
}
